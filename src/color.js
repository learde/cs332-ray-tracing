export class Color {
    constructor(r, g, b, a = 255) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    normalize() {
        this.r = Math.round(this.r);
        this.g = Math.round(this.g);
        this.b = Math.round(this.b);
        this.a = Math.round(this.a);
        this.clamp();
    }

    clamp() {
        this.r = Math.max(0, Math.min(255, this.r));
        this.g = Math.max(0, Math.min(255, this.g));
        this.b = Math.max(0, Math.min(255, this.b));
        this.a = Math.max(0, Math.min(255, this.a));
    }

    static gammaCorrection(c) {
        const r = Math.pow(c.r / 255, 0.45) * 255;
        const g = Math.pow(c.g / 255, 0.45) * 255;
        const b = Math.pow(c.b / 255, 0.45) * 255;
        return new Color(r, g, b, c.a);
    }

    static multiply(c1, intensity) {
        return new Color(c1.r * intensity, c1.g * intensity, c1.b * intensity, c1.a);
    }
}
