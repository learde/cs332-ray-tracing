import { Vec3 } from "./vec3.js";

export class Primitives {
    static sphere(ro, rd, ce, ra) {
        const oc = Vec3.sub(ro, ce);
        const b = Vec3.dot(oc, rd); 
        const c = Vec3.dot(oc, oc) - ra * ra;
        let h = b * b - c;
        if (h < 0) return [-1, -1];
        h = Math.sqrt(h);
        return [-b - h, -b + h];
    }
    
    static box(ro, rd, boxSize) {
        const m = Vec3.invert(rd);
        const n = Vec3.multiply(m, ro);
        const k = Vec3.multiply(Vec3.abs(m), boxSize);
        const t1 = Vec3.sub(Vec3.multiply(n, -1), k);
        const t2 = Vec3.add(Vec3.multiply(n, -1), k);
        const tN = Math.max(Math.max(t1.x, t1.y), t1.z);
        const tF = Math.min(Math.min(t2.x, t2.y), t2.z);
        if (tN > tF || tF < 0.0) return [-1, -1];
        let normal = (tN > 0.0) ? Vec3.step(new Vec3(tN, tN, tN), t1) :
                              Vec3.step(t2, new Vec3(tF, tF, tF));
        normal = Vec3.multiply(normal, Vec3.multiply(Vec3.sign(rd), -1));
        return [tN, tF, normal];
    }
}
