import { Vec3 } from "./vec3.js";
import { Color } from "./color.js";
import { Primitives } from "./primitives.js";

export class GL {
    constructor(id) {
        this.canvas = document.getElementById(id);
        this.ctx = this.canvas.getContext("2d");
        this.cw = this.canvas.width;
        this.ch = this.canvas.height;

        this.rayOrigin = new Vec3(0, 0, 0);

        this.vw = 1;
        this.vh = 1;
        this.d = 1;

        this.spheres = [];
        this.boxes = [];
    }

    render() {
        for (let i = -this.cw / 2; i < this.cw / 2; i++) {
            for (let j = -this.ch / 2; j < this.ch / 2; j++) {
                const [vx, vy, vz] = this.toViewPort(i, j, this.d);
                const rayDirection = new Vec3(vx - this.rayOrigin.x, vy - this.rayOrigin.y, vz - this.rayOrigin.z);
                rayDirection.normalize();

                const color = this.traceRay(rayDirection);

                this.setPixel(i, j, color);
            }
        }
    }

    toViewPort(x, y, z) {
        const newX = x * this.vw / this.cw + 0.0001;
        const newY = y * this.vh / this.ch + 0.0001;
        const newZ = z;

        return [newX, newY, newZ];
    } 

    traceRay(rayDirection, minT = 0, maxT = Infinity) {
        const lightPosition = new Vec3(0, 0.95, 3);
        let { color, p, normal } = this.castRay(this.rayOrigin, rayDirection, minT, maxT, lightPosition);

        // background
        if (color.r < 0) {
            return new Color(220, 220, 220);
        }

        const lightDirection = Vec3.sub(lightPosition, p);
        lightDirection.normalize();

        if (Vec3.dot(normal, lightDirection) > 0) {
            const r = this.castRay(p, lightDirection, minT+0.01, maxT, lightPosition);
            if (!r.hitRoom) {
                color = Color.multiply(color, 0.5);
            } 
        }

        return color;
    }

    castRay(rayOrigin, rayDirection, minT, maxT, lightPosition) {
        let closestT = Infinity;
        let closestObject = null;

        this.spheres.forEach(({ center, radius, color }) => {
            const [t1, t2] = Primitives.sphere(rayOrigin, rayDirection, center, radius);

            if (t1 >= minT && t1 <= maxT && t1 < closestT) {
                closestT = t1;

                const p = Vec3.multiply(rayDirection, t1);
                const normal = Vec3.sub(p, center);
                normal.normalize();
                const toLight = Vec3.sub(lightPosition, p);
                toLight.normalize();
                closestObject = { p, normal, color, toLight };
            }

            if (t2 >= minT && t2 <= maxT && t2 < closestT) {
                closestT = t2;

                const p = Vec3.multiply(rayDirection, t2);
                const normal = Vec3.sub(center, p);
                normal.normalize();
                const toLight = Vec3.sub(p, lightPosition);
                toLight.normalize();
                closestObject = { p, normal, color, toLight };
            }
        })

        this.boxes.forEach(({ origin, size, color, type }) => {
            let [t1, t2, normal] = Primitives.box(Vec3.sub(rayOrigin, origin), rayDirection, size);
            
            if (t1 >= minT && t1 <= maxT && t1 < closestT) {
                closestT = t1;
                const p = Vec3.multiply(rayDirection, t1);
                const toLight = Vec3.sub(lightPosition, p);
                toLight.normalize();
                normal.normalize();
                let hitRoom = false;
                if (type === "room") {
                    hitRoom = true;
                }
                closestObject = { p, normal, color, toLight, hitRoom };
            }

            if (t2 >= minT && t2 <= maxT && t2 < closestT) {
                closestT = t2;
                const p = Vec3.multiply(rayDirection, t2);
                const toLight = Vec3.sub(lightPosition, p);
                toLight.normalize();
                normal.normalize();
                let hitRoom = false;
                if (type === "room") {
                    if (normal.x === -1) {
                        color = new Color(102, 154, 204);
                    }
                    if (normal.x === 1) {
                        color = new Color(255, 51, 26);
                    }
                    hitRoom = true;
                }
                closestObject = { p, normal, color, toLight, hitRoom };
            }
        })

        if (closestObject !== null) {
            const diffuse = Math.max(0, Vec3.dot(closestObject.normal, closestObject.toLight)) * 0.5 + 0.3;
            const reflected = Vec3.sub(rayDirection, Vec3.multiply(closestObject.normal, 2 * Vec3.dot(closestObject.normal, rayDirection)));
            const specular = Math.pow(Math.max(0, Vec3.dot(reflected, closestObject.toLight)), 32) * 2;
            return {
                color: Color.multiply(Color.gammaCorrection(closestObject.color), (diffuse + specular) * 0.5),
                p: closestObject.p,
                normal: closestObject.normal,
                hitRoom: closestObject.hitRoom
            };
        }

        return { color: new Color(-1, -1, -1), p: null, normal: null };
    }

    addSphere(center, radius, color) {
        this.spheres.push({ center, radius, color });
    }
    
    addBox(origin, size, color, type) {
        this.boxes.push({ origin, size, color, type });
    }

    setPixel(x, y, color) {
        color.normalize();
        if (color.r < 0 || color.g < 0 || color.b < 0 || color.r > 255 || color.g > 255 || color.b > 255) debugger;
        this.ctx.fillStyle = 'rgba(' + color.r + ',' + color.g + ',' + color.b + ',' + (color.a / 255) + ')';
        this.ctx.fillRect((this.cw / 2 + x), (this.ch / 2 - y), 1, 1);
    }
}
