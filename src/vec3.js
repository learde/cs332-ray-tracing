export class Vec3 {
    constructor(x, y, z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    length() {
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }

    normalize() {
        const l = this.length();
        this.x /= l;
        this.y /= l;
        this.z /= l;
    }

    static toNormalized() {
        const v = new Vec3(this.x, this.y, this.z);
        v.normalize();
        return v;
    }

    static dot(v1, v2) {
        return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
    }

    static add(v1, v2) {
        return new Vec3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
    }

    static sub(v1, v2) {
        return new Vec3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
    }

    static multiply(v1, k) {
        if (typeof k === 'number') {
            return new Vec3(v1.x * k, v1.y * k, v1.z * k);
        }
        return new Vec3(v1.x * k.x, v1.y * k.y, v1.z * k.z);
    }

    static abs(v1) {
        return new Vec3(Math.abs(v1.x), Math.abs(v1.y), Math.abs(v1.z));
    }

    static step(edge, v1) {
        return new Vec3(
            (v1.x >= edge.x) ? 1.0 : 0.0,
            (v1.y >= edge.y) ? 1.0 : 0.0,
            (v1.z >= edge.z) ? 1.0 : 0.0
        );
    }

    static sign(v1) {
        return new Vec3(
            Math.sign(v1.x),
            Math.sign(v1.y),
            Math.sign(v1.z)
        );
    }

    static invert(v1) {
        const x = v1.x === 0.0 ? Infinity : 1.0 / v1.x;
        const y = v1.y === 0.0 ? Infinity : 1.0 / v1.y;
        const z = v1.z === 0.0 ? Infinity : 1.0 / v1.z;
        return new Vec3(x, y, z);
    }

    static equal(v1, v2) {
        return v1.x === v2.x && v1.y === v2.y && v1.z === v2.z;
    }
}
