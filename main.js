import { GL } from "./src/GL.js";
import { Vec3 } from "./src/vec3.js";
import { Color } from "./src/color.js";

window.onload = main;
const CANVAS_ID = "canvas";

function main() {
    const gl = new GL(CANVAS_ID);

    // gl.addSphere(new Vec3(2, 0, 4), 1, new Color(0, 255, 0));
    // gl.addSphere(new Vec3(0, 3, 10), 1, new Color(0, 0, 255));
    // gl.addSphere(new Vec3(0, 3, 10), 0.1, new Color(255, 255, 0));

    // object in room
    gl.addSphere(new Vec3(0.70, -0.75, 3), 0.25, new Color(133, 51, 255));
    gl.addSphere(new Vec3(0.1, -0.46, 2.75), 0.15, new Color(237, 145, 33));
    gl.addBox(new Vec3(-0.5, -0.7, 3), new Vec3(0.25, 0.5, 0.25), new Color(253, 233, 16));
    gl.addBox(new Vec3(0.1, -0.8, 2.7), new Vec3(0.2, 0.2, 0.2), new Color(11, 218, 81));

    // bulb
    // gl.addSphere(new Vec3(0, 1, 2), 0.05, new Color(0, 255, 0));
    
    // room
    gl.addBox(new Vec3(0, 0, 0), new Vec3(1, 1, 4), new Color(255, 255, 255), "room");

    gl.render(); 
}
